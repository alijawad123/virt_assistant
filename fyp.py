import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5 import uic
import textwrap
import queue
import concurrent.futures
import imutils
import pickle
import time
import cv2
import os
import face_recognition
import vosk
import pyttsx3
import sounddevice as sd
import webbrowser
import threading
import json
import win32api
import subprocess
import psutil
import time
import numpy as np
from time import sleep
import smtplib
import requests as req
from bs4 import BeautifulSoup
from datetime import datetime
import pyautogui
from email.message import EmailMessage
import geocoder
import random
import time
import wmi
import codecs
import wolframalpha
from word2number import w2n





sys.path.append(os.path.realpath('..'))




q = queue.Queue()
cascPathface = os.path.dirname(
     cv2.__file__) + "/data/haarcascade_frontalface_alt2.xml"
faceCascade = cv2.CascadeClassifier(cascPathface)
data = pickle.loads(open('face_enc', "rb").read())


def rec(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(gray,
                                         scaleFactor=1.1,
                                         minNeighbors=5,
                                         minSize=(60, 60),
                                         flags=cv2.CASCADE_SCALE_IMAGE)
 
    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    encodings = face_recognition.face_encodings(rgb)
    names = []
    found = False
    name = "Unknown"
    for encoding in encodings:
        matches = face_recognition.compare_faces(data["encodings"],
         encoding, 0.3)
        name = "Unknown"
        if True in matches:
            found = True
            matchedIdxs = [i for (i, b) in enumerate(matches) if b]
            counts = {}
            for i in matchedIdxs:
                name = data["names"][i]
                counts[name] = counts.get(name, 0) + 1
            name = max(counts, key=counts.get)

        names.append(name)
        for ((x, y, w, h), name) in zip(faces, names):
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)
            cv2.putText(frame, name, (x, y), cv2.FONT_HERSHEY_SIMPLEX,
             0.75, (255, 0, 0), 2)

    return found, frame, name



def Recognize():    
     
    print("Streaming started")
    video_capture = cv2.VideoCapture(0, cv2.CAP_DSHOW)
    future = [] 
    
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:    
        while True:

            # grab the frame from the threaded video stream
            ret, frame = video_capture.read()
            
            future.append(executor.submit(rec,(frame)))
            for fut in concurrent.futures.as_completed(future):
                if fut.result() != None:
                    return_value, frame, name = fut.result()
                    cv2.imshow("Frame", frame)
                    if return_value == True:
                        print("face detected for " + name)
                        video_capture.release()
                        cv2.destroyAllWindows()
                        return True
                    #rec(frame)

            
            
            keyCode = cv2.waitKey(33)
            if cv2.getWindowProperty('Frame' , 1) < 0:
                return False
                os._exit(1)
            #if cv2.waitKey(1) & 0xFF == ord('q'):
            #    break
        video_capture.release()
        cv2.destroyAllWindows()
        return False






class Dialog(QDialog):

    accepted = pyqtSignal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.date = QDateEdit()
        self.date.setDisplayFormat('MMM d, yyyy')
        self.date.setDate(QDate.currentDate())
        self.file_name = QLineEdit()
        self.printer_name = QLineEdit()
        self.printer_name.textEdited[str].connect(self.unlock)
        self.btn = QPushButton('OK')
        self.btn.setDisabled(True)
        self.btn.clicked.connect(self.ok_pressed)
        
        self.facebtn = QPushButton('Use Face Recogition')
        self.facebtn.clicked.connect(self.facerecog_pressed)


        form = QFormLayout(self)
        #form.addRow('Date', self.date)
        form.addRow('Username', self.file_name)
        form.addRow('Password', self.printer_name)
        form.addRow(self.btn)
        form.addRow(self.facebtn)

    def unlock(self, text):
        if text:
            self.btn.setEnabled(True)
        else:
            self.btn.setDisabled(True)

    def ok_pressed(self):
        #values = {'Date': self.date.date(),
         #       'File': self.file_name.text(),
          #     'Printer': self.printer_name.text()}
        print(self.file_name.text())
        print(self.printer_name.text())
        if self.file_name.text() == "root" and self.printer_name.text() == "toor":  
            self.accepted.emit()
            self.accept()
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Login Error!")
            msg.setText("Wrong Login Credentials")
            msg.exec_()
            exit(1)

    def closeEvent(self, event):
        exit(1)

    def facerecog_pressed(self):
        self.hide()
        #self.recognizer_thread = threading.Thread(target=Recognize)
        #self.recognizer_thread.start()

        #with concurrent.futures.ThreadPoolExecutor() as executor:
        #future = executor.submit(,)
        return_value = Recognize()#future.result()
        if return_value == True:
            self.accepted.emit()
            self.accept()
        else:
            os._exit(1)






class Message(QWidget):   #(QFrame): #
    def __init__(self, msg: str, *args, destroy_time: int = None, fg_color:str, bg_color:str):
        super().__init__(*args)

        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint) # <---
        self.setAttribute(Qt.WA_TranslucentBackground)                   # <---

        Colors_fg = fg_color
        Colors_bg = bg_color                         # <---

        # Main layout
        self.layout = QVBoxLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        limit = 40;
        final2 = "".join(msg)
        msg = '\n'.join(textwrap.wrap(final2, limit))

        nlines = msg.count('\n')

        self.height = 30 + nlines * 20

        label = QLabel(msg, alignment=Qt.AlignCenter)
        label.setFont(QFont("Helvetica Neue", 10, QFont.Bold, italic=True))       #(Fonts.text)
        label.setStyleSheet(f"color: {Colors_fg};"
                             "padding: 0px;")

        self.layout.addWidget(label)
        label.adjustSize()

        self.setStyleSheet(f"background-color: {Colors_bg};"  
                            "width:500px;"

                            "border-radius: 5px;"
                           )
        
    def getHeight(self):
        return self.height
    
class MyTableWidget(QWidget):
    
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.layout = QVBoxLayout()
        self.engine = pyttsx3.init()
        
        
        self.parent = parent
        self.w, self.h = self.parent.getSize()
        self.tabs = QTabWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()
        self.tabs.resize(parent.height,parent.width)
        
        self.tabs.addTab(self.tab1,"STT")
        self.tabs.addTab(self.tab2,"TTS")
        self.tabs.addTab(self.tab3,"About")


        self.tab1.setStyleSheet(f"background-color: #D4D4D4;")
        
        self.tab1.layout = QVBoxLayout(self)
        self.scrolwid = QScrollArea(self)
        self.wid = QWidget(self)

        self.layout_msgs = QGridLayout()
        self.wid.setLayout(self.layout_msgs)
        self.layout_msgs.setSpacing(2)
        self.wid.layout().setSizeConstraint(QLayout.SetFixedSize);
        self.num = 0

        self.user_bg = "rgba( 255, 255, 255, 255)"
        self.user_fg = "#000000"
        self.assistant_bg = "rgba( 0, 255, 0, 255)"
        self.assistant_fg = "#ffffff"
        
        self.scrolwid.setWidget(self.wid)
        self.scrolwid.setWidgetResizable(True);
        self.tab1.setLayout(self.tab1.layout)
        self.tab1.layout.addWidget(self.scrolwid)
        



        self.tab2.layout = QFormLayout(self)
        self.tab2.setLayout(self.tab2.layout)
        self.textBox = QLineEdit()
        self.btn = QPushButton("Say Text!")
        self.btn.clicked.connect(self.btnClicked)

        self.tab2.layout.addRow('Type in what you want the computer to say: ',self.textBox)
        self.tab2.layout.addRow('',self.btn)


        self.tab3.layout = QFormLayout(self)
        self.tab3.setLayout(self.tab3.layout)
        self.textBox1 = QLabel("This Software can be used to convert Speech to text and commands and to convert Text to Speech as well")
        self.tab3.layout.addWidget(self.textBox1)

        self.scrolwid.verticalScrollBar().rangeChanged.connect(lambda: self.scrolwid.verticalScrollBar().setValue(self.scrolwid.verticalScrollBar().maximum()))
        
 
        self.layout.addWidget(self.tabs)
        
        self.setLayout(self.layout)


    def addMessage(self, msg:str , isUser):
        if isUser == True:

            wid = Message(msg, fg_color = self.user_fg, bg_color = self.user_bg)
        
            wid.setFixedSize(self.w/2 - 40 ,wid.getHeight())
            
            self.layout_msgs.addWidget(wid,  self.num, 0)

        else:
            wid = Message(msg, fg_color = self.assistant_fg, bg_color = self.assistant_bg)
            wid.setFixedSize(self.w/2 - 40 ,wid.getHeight())
        
            self.layout_msgs.addWidget(wid,  self.num, 1)

        self.num = self.num + 1
        
        #print(self.wid.sizeHint().height())#self.scrolwid.verticalScrollBar().maximum())
        


             
    @pyqtSlot()
    def btnClicked(self):
        self.engine.say(self.textBox.text())
        self.engine.runAndWait()
        
    @pyqtSlot()
    def on_click(self):
        print("\n")
        for currentQTableWidgetItem in self.tableWidget.selectedItems():
            print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())



class App(QMainWindow):
    newMessage = pyqtSignal(str,int)

    def __init__(self):
        super().__init__()

        dg = Dialog()
        #dg.accepted.connect(self.do_something)
        dg.exec_()

        self.width = 650
        self.height = 700
        self.setFixedSize(self.width, self.height)

        self.title = 'AI Assistant'
        self.left = 0
        self.top = 0
        
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        self.table_widget = MyTableWidget(parent=self)

        self.setCentralWidget(self.table_widget)

        self.model = vosk.Model("model")
        self.engine = pyttsx3.init()
        self.browser = webbrowser.get('windows-default')
        self.email_addr = "t1e2s3t43214@gmail.com"
        self.psswrd = "Test1234Test"
        self.worker_thread = None
        self.record_thread = None
        self.run_event = threading.Event()
        self.text = ""
        self.active = False
        self.emailContent = ""
        self.Listening_Email = False
        self.writing = False
        self.file = None
        self.textContent = ""
        self.recording = threading.Event()
        self.recording_file_name = ""
        self.email_addresses = {}
        self.recepeint = ""
        self.get_contacts()
        self.StartListening()
        self.newMessage.connect(self.newMessageSlot)
        self.show()


    def getSize(self):
        return self.width, self.height    

    @pyqtSlot(str, int)
    def newMessageSlot(self,string,isUser):
        self.table_widget.addMessage(string, bool(isUser))

    @staticmethod
    def callback(indata, frames, time, status):
        """This is called (from a separate thread) for each audio block."""
        if status:
            print(status, file=sys.stderr)
        q.put(bytes(indata))

    def StartListening(self):
        if self.run_event.is_set():
            self.run_event.clear();
        if self.worker_thread != None:
            self.worker_thread.join();
        self.run_event = threading.Event()
        self.run_event.set()
        self.worker_thread = threading.Thread(target=self.worker_function)
        self.worker_thread.start()


    def closeEvent(self, event):
        self.run_event.clear();
        if self.worker_thread != None:
            self.worker_thread.join();

        self.recording.clear()
        if self.record_thread != None:
            self.record_thread.join()
        
        print("User has clicked the red x on the main window")


    def worker_function(self):
        try:
            dump_fn = None;
            device_info = sd.query_devices(None, 'input')   

            with sd.RawInputStream(samplerate=44100, blocksize = 18000, device=0, dtype='int16',
            channels=1, callback=self.callback):

                print("STARTED LISTENING...")

                rec = vosk.KaldiRecognizer(self.model, 44100)
                while self.run_event.is_set():
                        data = q.get()
                        if rec.AcceptWaveform(data):
                            
                            string = rec.Result();

                            parsed_json = json.loads(string)
                            if "hello" in parsed_json['text']:
                                self.text = parsed_json['text']
                                self.active = True                                
                                try:

                                    self.generate_response()
                                except Exception as exp:
                                    print(exp)
                                    self.close()
                                    return

                                continue


                            if self.active == True:
                                if self.Listening_Email == False:
                                    
                                    self.text = parsed_json['text']
                                    try:

                                        self.generate_response()
                                    except Exception as exp:
                                        print(exp)
                                        self.close()
                                        return
                                else:
                                    self.text = parsed_json['text']
                                    self.emailContent = self.emailContent + " " + self.text
                                    try:

                                        self.generate_response()
                                    except Exception as exp:
                                        print(exp)
                                        self.close()
                                        return



                        if dump_fn is not None:
                            dump_fn.write(data)

                return
        except KeyboardInterrupt:
            return

    def get_contacts(self, filename = "emails.txt"):
        with open(filename, mode='r', encoding='utf-8') as contacts_file:
            for a_contact in contacts_file:
                self.email_addresses[a_contact.split()[0]] = a_contact.split()[1]



    def generate_response(self):

        if self.Listening_Email == False and self.writing == False:
            
            print(self.text)
            
            if "hello" in self.text:
                now = datetime.now()
                text = "Good " + self.get_part_of_day(now.hour)
                text = text + " Buddy! What can I do for you today?"
                self.newMessage.emit(self.text, 1)
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                
                self.text = ""



            if "how are you" in self.text:

                text = "I am well"
                self.newMessage.emit(self.text, 1)
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                self.text = ""
                
            if "what time" in self.text:

                
                now = datetime.now()
                text = "It is " + str(now.hour) + "hours and" + str(now.minute) + "minutes"
                self.newMessage.emit(self.text, 1)
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                self.text = ""

            if "what date" in self.text:

                
                now = datetime.now()
                text = "It is " + str(now.month)  + "month and "+ (now.day) + "day today"
                self.newMessage.emit(self.text, 1)
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                self.text = ""

            if "where is" in self.text:

                textList = self.text.split(" ")
                location_url = "https://www.google.com/maps/place/" + str(textList[2])
                text = "Let me google where" + textList[2] + " is."
                self.newMessage.emit(self.text, 1)
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                self.text = ""
                self.browser.open(location_url)

                
                
            if any(x in self.text for x in ["what is the weather in", "how is the weather in", "how's the weather in"]):

                text = "wait let me google that for you"
                self.newMessage.emit(self.text, 1)
                self.newMessage.emit(text, 0)
                    
                weather_url = "https://www.google.com/search?q="
                if len(self.text.split("weather in ")) == 1:
                    self.newMessage.emit("Location not given...", 0)
                    self.engine.say("Location not given...")
                    self.engine.runAndWait()
                    return
                url = weather_url + self.text.split("weather in ")[1].replace(" ", "+")
                self.browser.open(url)
                resp = req.get("https://www.google.com/search?q=weather+in+pakistan&oq=weather+in+pakistan")
                soup = BeautifulSoup(resp.content, 'html.parser')
                mydivs = soup.find_all("div", {"class": "BNeawe iBp4i AP7Wnd"})
                self.newMessage.emit("it is " + mydivs[0].text + "in " + self.text.split("weather in ")[1], 0)
                self.engine.say("it is " + mydivs[0].text + "in " + self.text.split("weather in ")[1])
                self.engine.runAndWait()
            
                self.text = ""

            if "seach on wolfram alpha" in self.text:
                # Taking input from user
                if len(self.text.split("seach on wolfram alpha ")) == 1:
                    self.newMessage.emit("Expression not given", 0)
                    self.engine.say("Expression not given")
                    self.engine.runAndWait()
                    return
                question = self.text.split("seach on wolfram alpha ")[1]
                app_id = "6AG5TR-9YVVWV4W3Q"
                client = wolframalpha.Client(app_id)
                res = client.query(question)
                answer = next(res.results).text
                self.newMessage.emit(self.text, 1)
                self.newMessage.emit(answer, 0)
                self.engine.say(answer)
                self.engine.runAndWait()
                print(answer)
            


            if "stop listening" in self.text:


                self.newMessage.emit(self.text, 1)
                text = 'Listening Stopped'
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                
                self.text = ""

                self.active = False

            if any(x in self.text for x in ["write an email to", "right an email to", "write email to", "right email to", "send an email to", "send email to"]):
                self.newMessage.emit(self.text, 1)
                if len(self.text.split("email to ",1)) > 1:
                    temp = self.text.split("email to ",1)[1]
                else:
                    self.newMessage.emit("Recepeint wasnt named", 0)
                    self.engine.say("Recepeint wasnt named")
                    self.engin.runAndWait()
                    return

                
                self.text.replace("right", "write")
                self.recepeint = temp
                self.email_recepient = self.email_addresses.get(temp)
                if self.email_recepient == None:
                    text = 'Sorry Could not find the recepeint'
                    self.newMessage.emit(text, 0)
                    self.engine.say(text)
                    self.engine.runAndWait()
                    self.text = ""
                    
                    return


                self.Listening_Email = True
                text = 'Please dictate the email'
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                self.text = ""
                return

            if "play music" in self.text:
                self.newMessage.emit(self.text, 1)
                text = "Playing Music Now"
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()

                drives = win32api.GetLogicalDriveStrings()
                drives = drives.split('\000')[:-1]
                driveIndex = os.system("wmic OS GET SystemDrive /VALUE")
                window_drive = drives[driveIndex]
                music = []
                for drive in drives:
                    if drive == window_drive:
                        continue
                    if drive == None:
                        continue

                    music_temp = self.findMusic(drive.split("\\")[0] + "\\")
                    if music !=None:
                    	music.append(music_temp)


                if len(music) != 0:
                    self.media_player_proc = subprocess.Popen([music[0]],shell=True) 
                    return
                else:

	                self.newMessage.emit("Could not find music on your device", 0)
	                self.engine.say("Could not find music on your device")
	                self.engine.runAndWait()
	                self.text = ""

   

                

                

            if "find" in self.text  or "search" in self.text:




                if "youtube" in self.text or "you tube" in self.text:
                    text = "Here are your results"
                    self.newMessage.emit(self.text, 1)
                    self.engine.say(text)
                    self.engine.runAndWait()
                    self.newMessage.emit(text, 0)
                    
                    weather_url = "https://www.youtube.com/results?search_query="



                    if "find" in self.text:
                        text = self.text.split("find ")[1]                        
                    else:
                        text = self.text.split("search ")[1]


                    if "youtube" in self.text:
                        text = text.split(" on youtube")[0]
                    if "you tube" in self.text:
                        text = text.split(" on you tube")[0]

                    url = weather_url + text.replace(" ", "+")
                    
                    self.browser.open(url)
                    self.text = ""




                elif "wikipedia" in self.text:
                    self.newMessage.emit(self.text, 1)
                    text = "Here are your results"
                    self.newMessage.emit(text, 0)

                    self.engine.say(text)
                    self.engine.runAndWait()
                    weather_url = "https://en.wikipedia.org/wiki/"
                    if "find" in self.text:
                        text = self.text.split("find ")[1]
                    else:
                        text = self.text.split("search")[1]

                    text = text.split("on wikipedia")[0]

                    url = weather_url + text.replace(" ", "_")


                    self.browser.open(url)
                    self.text = ""
                    string = ""

                    page = req.get(url)
                    soup = BeautifulSoup(page.content, 'html.parser')
                    try:
                        txt = soup.find("div", {"class": "mw-parser-output"})
                        children = txt.findChildren("p" , recursive=False)
                        children.extend(txt.findChildren("a", recursive=False))

                        
                        for child in children:
                            string = string + " " + child.text

                        self.engine.say(''.join(string.split(".")[:2]))
                        self.engine.runAndWait()
                        self.newMessage.emit(''.join(string.split(".")[:2]), 0)
                        print(string)

                    except Exception as exp:
                        pass

                elif "on google" in self.text:

                    text = "Googling right away"
                    self.newMessage.emit(self.text, 1)
                    self.newMessage.emit(text, 0)
                    self.engine.say(text)
                    self.engine.runAndWait()
                        
                    google_url = "https://www.google.com/search?q="

                    if "search" in self.text:
                        text = self.text.split("search ")[1]
                    else:
                        url = self.text.split("find ")[1]

                    text = text.split(" on google")[0]

                    url = google_url + text.replace(" ", "+")
                    self.browser.open(url)          
                    self.text = ""  



                elif "my computer" in self.text:
                    self.newMessage.emit(self.text, 1)
                    text = "Finding on your whole device. Please wait..."
                    self.newMessage.emit(text, 0)
                    self.engine.say(text)
                    self.engine.runAndWait()
                    
                    res = []
                    to_find = self.text.split("find ",1)[1]
                    drives = win32api.GetLogicalDriveStrings()
                    drives = drives.split('\000')[:-1]
                    for drive in drives:
                        res.extend(self.find(to_find, drive))

                    if len(res) == 0:
                        text ="Could not find the file in your computer"
                        self.newMessage.emit(text, 0)
                        self.engine.say(text)
                        self.engine.runAndWait()
                        self.text = ""

                    else:
                        text = "Your pattern matched the following files"#.join(res)
                        print(res)
                        self.newMessage.emit(text, 0)
                        self.engine.say(text)
                        self.engine.runAndWait()




            if any(x in self.text for x in ["write text", "right text"]):
                self.newMessage.emit(self.text, 1)
                self.writing = True
                text = 'Please dictate'
                self.newMessage.emit(text,0)
                self.engine.say(text)
                self.engine.runAndWait()
                self.text = ""



            if "launch " in self.text or "open" in self.text:
                self.newMessage.emit(self.text, 1)
                to_launch = ""

                if "launch" in self.text:
                    if len(self.text.split("launch ")) > 1:
                        to_launch = self.text.split("launch ")[1]
                    else:
                        text = 'Unable to find program'
                        self.newMessage.emit(text,0)
                        self.engine.say(text)
                        self.engine.runAndWait()
                        return


                elif "open" in self.text:
                    if len(self.text.split("open ")) > 1:
                        to_launch = self.text.split("open ")[1]
                    else:
                        text = 'Unable to find program'
                        self.newMessage.emit(text,0)
                        self.engine.say(text)
                        self.engine.runAndWait()
                        return




                ret = self.runProgram(to_launch)
                if ret != None:
                    os.system(ret)
                    text = 'Launching Program '
                    self.newMessage.emit(text,0)
                    self.engine.say(text)
                    self.engine.runAndWait()
                    return
                else:
                    text = 'Unable to find program'
                    self.newMessage.emit(text,0)
                    self.engine.say(text)
                    self.engine.runAndWait()
                    return



            if "where am i" in self.text or "my location" in self.text:
                self.newMessage.emit(self.text, 1)
                
                text = "Getting Your Location Now"
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                g = geocoder.ip('me')
                print(g.latlng)
                url = "https://www.google.com/maps/search/"+ str(g.lat) + "," + str(g.lng)
                self.browser.open(url)
                self.text = ""

            if any(x in self.text for x in ["what's", "what is"]) or any(x in self.text for x in ["plus","minus", "multiply", "divide"]):
                self.newMessage.emit(self.text, 1)
                self.calc()
                

            if "tell a joke" in self.text:
                self.newMessage.emit(self.text, 1)

                f = open("jokes.txt", "r",  encoding="utf8")
                jokes = f.read().split("\n")
                to_tell = jokes[random.randint(0,len(jokes))].split("<>")
                self.engine.say(to_tell[0])
                self.engine.runAndWait()
                time.sleep(1)
                self.engine.say(to_tell[1])
                self.engine.runAndWait()
                self.newMessage.emit(to_tell[0] + to_tell[1], 0)


            if "battery status" in self.text:
                self.newMessage.emit(self.text, 1)

                pl = subprocess.Popen(['powershell', '(Get-WmiObject Win32_Battery).EstimatedChargeRemaining'], stdout=subprocess.PIPE).communicate()[0]

                text = 'RemainingCapacity: ' + str(pl.decode('utf-8'))
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()


            if "start recording" in self.text:
                self.newMessage.emit(self.text, 1)
                if self.recording.is_set():
                    text = 'Already Recording'
                    self.newMessage.emit(text, 0)
                    self.engine.say(text)
                    self.engine.runAndWait()
                    return

                d = datetime.now()
                name = d.strftime("%m/%d/%Y, %H:%M:%S").replace("/", "_").replace(":","_").replace(", ","")
                text = 'Recording Started'
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()
                self.recording.set()
                self.recording_file_name = name
                self.record_thread = threading.Thread(target = self.screen_record)
                self.record_thread.start()

            if "stop recording" in self.text:
                self.newMessage.emit(self.text, 1)
                self.newMessage.emit("Recording Stopped", 0)
                self.recording.clear()
                self.record_thread.join()
                

                text = "recording saved in file " + self.recording_file_name
                self.newMessage.emit(text, 0)
                self.engine.say(text)
                self.engine.runAndWait()


            if "take a screen shot" in self.text or "take a screenshot" in self.text:
                self.newMessage.emit(self.text, 1)
                myScreenshot = pyautogui.screenshot()
                d = datetime.now()
                name = d.strftime("%m/%d/%Y, %H:%M:%S").replace("/", "_").replace(":","_").replace(", ","")
                myScreenshot.save(name + ".png")
                text = "screenshot saved in file " + name + ".png"
                self.newMessage.emit(text, 0)
                self.engine.say("Screenshot Saved")
                self.engine.runAndWait()

            if "shut down" in self.text:
                self.newMessage.emit(self.text, 1)
                if "in" in self.text:
                    sec = self.text.split("in ")[1]

                strOne = "shutdown /p"
                text = "Shutting down now"
                self.engine.say(text , 0)
                self.newMessage.emit(text, 0)
                self.engine.runAndWait()
                os.system(strOne)
                

            if "restart" in self.text:
                self.newMessage.emit(self.text, 1)
                sec = ""
                if "in" in self.text:
                    sec = self.text.split("in ")[1]

                strOne = "shutdown /r /t 0 "
                text = "Restarting now"
                self.engine.say(text)
                self.engine.say(text)
                self.engine.runAndWait()
                os.system(strOne)
                




        elif self.Listening_Email == True:      
            print(self.emailContent)
            if "stop stop" in self.text:
                self.emailContent = self.emailContent.split(" stop stop ")[0]
                self.Listening_Email = False
                s = smtplib.SMTP('smtp.gmail.com', 587)

                msg = EmailMessage()
                msg.set_content(self.emailContent)

                msg['Subject'] = 'Hello '+ self.recepeint
                msg['From'] = self.email_addr
                msg['To'] = self.email_recepient

                s.starttls()
                s.login(self.email_addr, self.psswrd)

                

                s.send_message(msg)
                s.quit()


                self.emailContent = ""
                text = "Email sent!"
                self.engine.say(text)
                self.engine.runAndWait()
                self.newMessage.emit(text, 0)


        elif self.writing == True:
            self.textContent = self.textContent + " " + self.text
            if "stop stop" in self.text:
                self.textContent = self.textContent.split("stop stop ",1)[0]
                self.writing = False
                
                f = open(datetime.now() + ".txt", "a")
                f.write(self.textContent)
                f.close()
                self.engine.say("Text saved in file " + datetime.now() + ".txt")
                self.engine.runAndWait()
                self.textContent = ""
                self.text = ""



    def screen_record(self):
        fourcc = cv2.VideoWriter_fourcc(*"XVID")
        out = cv2.VideoWriter(self.recording_file_name + ".avi", fourcc, 25.0, (pyautogui.size()))
        while self.recording.is_set():
            img = pyautogui.screenshot()
            frame = np.array(img)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            out.write(frame)
        out.release()


    def find(self, pattern, path):
        result = []
        for root, dirs, files in os.walk(path):
            for name in files:
                if pattern in name:
                    result.append(os.path.join(root, name))
                    if len(result) >= 10:
                        return result
        return result

    def findMusic(self, path):
        for root, dirs, files in os.walk(path):
            for name in files:
                if os.path.splitext(name)[1] == ".mp3" and not "$" in os.path.join(root, name):
                    print(os.path.join(root, name))
                    return os.path.join(root, name)


    def runProgram(self, name):
        dic = {}
        pl = subprocess.Popen(['powershell', 'Get-StartApps > temp.txt'], stdout=subprocess.PIPE).communicate()[0]
        with codecs.open('temp.txt', encoding='utf-16') as f:

            for x in f.readlines():
                if len(x) > 2:
                    software = ' '.join(x.split()[:-1])
                    software = software.lower()
                    path = ''.join(x.split()[-1])
                    if name == software:
                        text = 'start explorer shell:appsfolder\\' + path
                        return text


    def get_part_of_day(self, hour):
        return (
            "morning" if 5 <= hour <= 11
            else
            "afternoon" if 12 <= hour <= 17
            else
            "evening" if 18 <= hour <= 22
            else
            "night"
        )


    def calc(self):
        text = self.text
        dictionary = {"plus" : "+", "minus" : "-" , "multiply": "*", "divide": "/", "deivided": "/", "multiplied": "*"}
        if "what is " in self.text:
            text = self.text.split("what is ")[1]
        if "what's " in self.text:
            text = self.text.split("what's ")[1]


        text = text.replace(" by "," ")


        tokens = text.split()
        p = ""
        fin = []
        for t in range(0,len(tokens)):
            if tokens[t] in dictionary:
                if len(p) != 0:
                    try:
                        fin.append(str(w2n.word_to_num(p)))
                    except Exception as exp:
                        text = "Could not compute"
                        self.newMessage.emit(text, 0)
                        self.engine.say(text)
                        self.engine.runAndWait()
                        print(text)
                        return

                fin.append(dictionary[tokens[t]])
                p = ""


            else:
                p = p + " " + tokens[t]



        print("Hello")
        try:
            fin.append(str(w2n.word_to_num(p)))
        except Exception as exp:
            text = "Could not compute"
            self.newMessage.emit(text, 0)
            self.engine.say(text)
            self.engine.runAndWait()
            print(text)
            return



        text = ''.join(fin)


        try:
            text = str(eval(text)) 
            self.newMessage.emit(text, 0)
            self.engine.say(text)
            self.engine.runAndWait()
            print(text)
        except:
            text = "Could not compute"
            self.newMessage.emit(text, 0)
            self.engine.say(text)
            self.engine.runAndWait()
            print(text)


if __name__ == '__main__':
    try:
        app = QApplication(sys.argv)
        ex = App()
        sys.exit(app.exec_())
        import signal

        signal.signal(signal.SIGINT, signal.SIG_DFL)
    except KeyboardInterrupt:
        os.e_exit(0)

